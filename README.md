# Game Server

#### Getting Started

Clone the repo.

```
git clone https://gitlab.com/nsa.ganeshkumar/game-server-test.git
```

Application config properties are in `resources/applications.properties`.

#### Assumptions made:

1. No changes has been made the GameServer interface provided by King originally. The implementation sticks to that spec.
2. The Login API generates token based on username but other APIs don't validate it based on the username.
3. High scores are ordered based on `scores` in descending order, if more than one user has same score, then results are ordered by natural sort order of `username`.
4. High scores are returned in the format of `<username>-<score>,<username>-<score>,...`

#### Tests

There are enough test coverage for GameServer, LoginService and Concurrency. Tests are located under `test` folder.



 