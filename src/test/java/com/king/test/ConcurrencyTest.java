package com.king.test;

import com.king.test.api.configuration.AppConfig;
import com.king.test.api.model.UserScore;
import com.king.test.api.service.GameServer;
import com.king.test.api.service.GameServerImpl;
import com.king.test.api.service.UserScoreDataService;
import org.junit.Test;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class ConcurrencyTest extends BaseTest {

    private static Logger logger = LoggerFactory.getLogger(ConcurrencyTest.class);

    private GameServer gameServer = new GameServerImpl();

    @BeforeAll
    void setup() {
        AppConfig.SESSION_KEY_VALID_SECONDS = 600;
    }

    @BeforeEach
    void reset() throws NoSuchFieldException, IllegalAccessException {
        resetUserScoreData();
    }

    // Candidate for a Parameterized Test to run with diff threadCount, taskCount, userCount, levelCount etc.
    @DisplayName("Test with concurrent threads that update score for same user and level")
    @Test
    public void tetConcurrency() throws InterruptedException, ExecutionException {

        int threadCount = 1000;
        int taskCount = 10000;
        int userCount = 10;
        int levelCount = 100;

        CountDownLatch blockingLatch = new CountDownLatch(1);
        CountDownLatch allTasksFinishedLatch = new CountDownLatch(taskCount);

        Callable<UserScore> task = () -> {
            Random random = new Random();
            String username = String.format("user%02d", random.nextInt(userCount));

            UserScore userScore = new UserScore(username, random.nextInt(levelCount), random.nextInt(5000));

            String sessionId = gameServer.login(username);
            // Just to create some overlap between Threads
            try {
                blockingLatch.await();

                gameServer.submitScore(sessionId, userScore);
                allTasksFinishedLatch.countDown();
            } catch (Exception e) {
                logger.error("Error occurred in test while submitting score for user={} level={} score={}",
                                        userScore.getUsername(), userScore.getLevel(), userScore.getScore(), e);
            }

            return userScore;
        };

        List<Future<UserScore>> futures = new ArrayList<>();
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
        IntStream.range(0, taskCount).forEach( (i) -> {
                futures.add(executorService.submit(task));
            }
        );

        blockingLatch.countDown();
        allTasksFinishedLatch.await();

        // Prepare the test user-scores Map by level and username
        Map<Integer, Map<String, List<UserScore>>> generatedTestData = collectTestData(futures);

        // Assert the test data against UserScoreDataService
        assertResults(generatedTestData);
    }

    private void assertResults(Map<Integer, Map<String, List<UserScore>>> generatedTestData) {
        generatedTestData.forEach((level, testUserScoresDataMap) -> {
            UserScoreDataService.getInstance().getUserScoresForLevel(level).ifPresent(userScores -> {
                userScores.forEach((userScore) -> {
                    List<UserScore> testScoresData = testUserScoresDataMap.get(userScore.getUsername());

                    UserScore topScore = null;
                    if (testScoresData != null) {
                        topScore = testScoresData.stream()
                                .max(Comparator.comparing(UserScore::getScore))
                                .orElseGet(() -> null);
                    }
                    Assertions.assertEquals(topScore, userScore);
                });
            });
        });
    }

    private Map<Integer, Map<String, List<UserScore>>> collectTestData(List<Future<UserScore>> futures) throws ExecutionException, InterruptedException {
        Map<Integer, Map<String, List<UserScore>>> generatedTestData = new HashMap<>();
        for (Future<UserScore> testUserScore : futures) {
            UserScore userScore = testUserScore.get();

            Map<String, List<UserScore>> userScoresMap = generatedTestData.computeIfAbsent(userScore.getLevel(), (level) -> new HashMap<>());
            List<UserScore> scores = userScoresMap.computeIfAbsent(userScore.getUsername(), (name) -> new ArrayList<>());
            scores.add(userScore);
        }

        return generatedTestData;
    }

    @AfterAll
    void cleanUp() {
        AppConfig.SESSION_KEY_VALID_SECONDS = 5;
    }
}
