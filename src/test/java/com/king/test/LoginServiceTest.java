package com.king.test;

import com.king.test.api.exception.InvalidSessionException;
import com.king.test.api.configuration.AppConfig;
import com.king.test.api.service.LoginService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LoginServiceTest {

    @DisplayName("Test if unique session ids are generated and has no strange characters")
    @Test
    public void testCreateSession() {
        // Create 10 Session IDs for same user
        Set<String> sessionIds = IntStream.range(1,11)
                .mapToObj(index -> LoginService.createNewSession("user01"))
                .collect(Collectors.toSet());

        // Expect unique tokens generated each time
        Assertions.assertEquals(10, sessionIds.size());

        // None of them should contain strange characters
        Assertions.assertEquals(0, sessionIds.stream()
                .filter(sessionId -> !validateIfSessionIdHasNoStrangeChars(sessionId))
                .count());
    }

    @DisplayName("Test if validate session id throws exception for any invalid or null string")
    @Test
    public void testValidateSessionId() {
        Assertions.assertThrows(InvalidSessionException.class, () -> {
            LoginService.validateSessionId("weiruweoiruwedsjflks");
        });

        Assertions.assertThrows(InvalidSessionException.class, () -> {
            LoginService.validateSessionId(null);
        });
    }

    @DisplayName("Test if session id is valid only for certain period")
    @Test
    public void testSessionIdValidityPeriod() throws Exception {
        String sessionId = LoginService.createNewSession("user01");

        TimeUnit.SECONDS.sleep(AppConfig.SESSION_KEY_VALID_SECONDS);

        Assertions.assertThrows(InvalidSessionException.class, () -> {
            LoginService.validateSessionId(sessionId);
        });
    }

    private boolean validateIfSessionIdHasNoStrangeChars(String str) {
        Pattern pattern = Pattern.compile("[a-zA-Z0-9]*");
        Matcher matcher = pattern.matcher(str);

        return matcher.matches();
    }
}

