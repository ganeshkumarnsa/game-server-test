package com.king.test;

import com.king.test.api.service.GameServer;
import com.king.test.api.service.GameServerImpl;
import com.king.test.api.exception.InvalidSessionException;
import com.king.test.api.model.UserScore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

class GameServerTest extends BaseTest {

    private Logger logger = LoggerFactory.getLogger(GameServerTest.class);

    private GameServer gameServer = new GameServerImpl();

    @BeforeEach
    void setup() throws NoSuchFieldException, IllegalAccessException {
        resetUserScoreData();
    }

    @DisplayName("Test if high-scores are returned in descending order of the score and one user appears only once in the list")
    @Test
    void testGetHighScores() throws InvalidSessionException {
        String sessionId = gameServer.login("user01");

        gameServer.submitScore(sessionId, new UserScore("user01", 1, 4));
        gameServer.submitScore(sessionId, new UserScore("user02", 1, 2));
        gameServer.submitScore(sessionId, new UserScore("user03", 1, 1));
        gameServer.submitScore(sessionId, new UserScore("user01", 1, 5));
        gameServer.submitScore(sessionId, new UserScore("user04", 1, 10));
        gameServer.submitScore(sessionId, new UserScore("user04", 1, 6));
        gameServer.submitScore(sessionId, new UserScore("user05", 1, 8));
        gameServer.submitScore(sessionId, new UserScore("user06", 1, 3));
        gameServer.submitScore(sessionId, new UserScore("user04", 1, 8));

        String highScores = gameServer.getHighScores(sessionId, 1);

        Assertions.assertEquals("user04-10,user05-8,user01-5,user06-3,user02-2,user03-1", highScores);
    }

    @DisplayName("Test for every user only high scores are stored at each level")
    @Test
    void testIfOnlyHighScoreIsStored() throws InvalidSessionException {
        String sessionId = gameServer.login("user01");

        gameServer.submitScore(sessionId, new UserScore("user01", 1, 1));
        gameServer.submitScore(sessionId, new UserScore("user01", 1, 2));
        gameServer.submitScore(sessionId, new UserScore("user01", 1, 5));
        gameServer.submitScore(sessionId, new UserScore("user01", 3, 4));
        gameServer.submitScore(sessionId, new UserScore("user01", 3, 5));
        gameServer.submitScore(sessionId, new UserScore("user01", 3, 10));

        String highScores = gameServer.getHighScores(sessionId, 1);
        Assertions.assertEquals("user01-5", highScores);

        highScores = gameServer.getHighScores(sessionId, 3);
        Assertions.assertEquals("user01-10", highScores);
    }

    @DisplayName("Test if empty string is returned for a given level if no scores are available for that level")
    @Test
    void testGetHighScores_ReturnsEmpty() throws InvalidSessionException {
        String sessionId = gameServer.login("user01");

        String highScores = gameServer.getHighScores(sessionId, 10);

        Assertions.assertEquals("", highScores);
    }

    @DisplayName("Test if only top 15 scores are returned")
    @Test
    void testTopHighScoresLimit() throws InvalidSessionException {
        // No authorization done based on username.
        // so one sessionId is enough to submit scores for all users
        String sessionId = gameServer.login("user01");

        // Here we are submitting scores from 1 to 30 for 5 levels, for 30 users.
        // GameServer should keep only the high score i.e. top-score should be 30 for all users for all levels.

        IntStream.range(1, 5).forEach(level -> { // 5 Levels
            IntStream.range(1, 31).forEach((userNo) -> { // 30 users from user01 to user30
                IntStream.range(1, 31)
                    .mapToObj(score -> new UserScore(String.format("user%02d", userNo), level, score))
                    .forEach(userScore -> {
                            try {
                                gameServer.submitScore(sessionId, userScore);
                            } catch (InvalidSessionException e) {
                                logger.error("Error occurred when submitting score for user={} level={} score={}",
                                                userScore.getUsername(), userScore.getLevel(), userScore.getScore(), e);
                            }
                        }
                    );
            });
        });

        // Only top 15 scores should be returned
        String highScores = gameServer.getHighScores(sessionId, 1);

        // Top score should be 30 every user.
        // Only user01 to user15 should be returned in the high-scores list
        // Result is ordered based on score's descending order and then username's natural order (if more than one user has same score at a level).
        String expectedResult = IntStream.range(1, 16)
                                            .boxed()
                                            .map(userNo -> String.format("user%02d-30", userNo))
                                            .collect(Collectors.joining(","));

        Assertions.assertEquals(expectedResult, highScores);
    }
}
