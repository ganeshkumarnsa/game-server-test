package com.king.test;

import com.king.test.api.service.UserScoreDataService;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class BaseTest {

    void resetUserScoreData() throws NoSuchFieldException, IllegalAccessException {
        Field instance = UserScoreDataService.class.getDeclaredField("levelVsUserScores");
        instance.setAccessible(true);
        instance.set(UserScoreDataService.getInstance(), new ConcurrentHashMap<>());
    }
}
