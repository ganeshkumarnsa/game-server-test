package com.king.test.api.configuration;

import java.util.Properties;

public class AppConfig {

    public static int SESSION_KEY_VALID_SECONDS;
    public static int HIGH_SCORES_LIMIT;

    static {
        try {
            Properties properties = new Properties();
            properties.load(ClassLoader.getSystemResourceAsStream("application.properties"));
            init(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void init(Properties properties) {
        SESSION_KEY_VALID_SECONDS = Integer.parseInt(properties.getProperty("king.game.session.validity.seconds"));
        HIGH_SCORES_LIMIT = Integer.parseInt(properties.getProperty("king.game.highscores.limit"));
    }
}
