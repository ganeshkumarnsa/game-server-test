package com.king.test.api.service;

import com.king.test.api.model.UserScore;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class UserScoreDataService {

    private static final UserScoreDataService userScoreDataService = new UserScoreDataService();

    private Map<Integer, Map<String, UserScore>> levelVsUserScores = new ConcurrentHashMap<>();

    private UserScoreDataService() {
    }

    public static UserScoreDataService getInstance() {
        return userScoreDataService;
    }

    public void insertOrUpdateScore(UserScore newScore) {
        Map<String, UserScore> userScoreMap =
                levelVsUserScores.computeIfAbsent(newScore.getLevel(), level -> new ConcurrentHashMap<>());

        userScoreMap.compute(newScore.getUsername(), (key, oldValue) -> {
            if (oldValue == null || newScore.getScore() > oldValue.getScore()) {
                return newScore;
            }
            return oldValue;
        });
    }

    public Optional<Collection<UserScore>> getUserScoresForLevel(int level) {
        return Optional.ofNullable(this.levelVsUserScores.get(level)).map(Map::values);
    }
}
