package com.king.test.api.service;

import com.king.test.api.exception.InvalidSessionException;
import com.king.test.api.configuration.AppConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.UUID;

public class LoginService {

    private static Logger logger = LoggerFactory.getLogger(LoginService.class);

    public static String createNewSession(String username) {
        String validUntil = String.valueOf(Instant.now().plus(AppConfig.SESSION_KEY_VALID_SECONDS, ChronoUnit.SECONDS).toEpochMilli());
        String uniqueSessionId = String.join(";", UUID.randomUUID().toString(), username, validUntil);

        // We can do AES encryption here for Production security.

        // Do Base64 encode to eliminate having strange characters
        return Base64.getEncoder().encodeToString(uniqueSessionId.getBytes());
    }

    public static boolean validateSessionId(String sessionId) throws InvalidSessionException {
        try {
            String decodedKey = new String(Base64.getDecoder().decode(sessionId));

            String[] parts = decodedKey.split(";");

            Instant validUntil = Instant.ofEpochMilli(Long.valueOf(parts[2]));

            if (validUntil.compareTo(Instant.now()) < 0) {
                logger.error("User={} Session ID expired", parts[1]);
                throw new InvalidSessionException();
            }
        } catch (Exception e) {
            logger.error("Invalid Session ID {}", sessionId);
            throw new InvalidSessionException();
        }

        return true;
    }
}
