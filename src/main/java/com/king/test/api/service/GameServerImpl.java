package com.king.test.api.service;

import com.king.test.api.model.UserScore;
import com.king.test.api.configuration.AppConfig;
import com.king.test.api.exception.InvalidSessionException;

import java.util.*;
import java.util.stream.Collectors;

public class GameServerImpl implements GameServer {

    private UserScoreDataService userScoreDataService;

    private Comparator<UserScore> scoreComparator = Comparator.comparing(UserScore::getScore).reversed();
    private Comparator<UserScore> nameComparator =  Comparator.comparing(UserScore::getUsername);

    public GameServerImpl() {
        this.userScoreDataService = UserScoreDataService.getInstance();
    }

    @Override
    public String login(String username) {
        return LoginService.createNewSession(username);
    }

    @Override
    public void submitScore(String sessionKey, UserScore newScore) throws InvalidSessionException {
        LoginService.validateSessionId(sessionKey);

        this.userScoreDataService.insertOrUpdateScore(newScore);
    }

    @Override
    public String getHighScores(String sessionKey, int level) throws InvalidSessionException {
        LoginService.validateSessionId(sessionKey);

        return this.userScoreDataService.getUserScoresForLevel(level).map(userScores ->
                userScores.parallelStream()
                        .sorted(scoreComparator.thenComparing(nameComparator))
                        .limit(AppConfig.HIGH_SCORES_LIMIT)
                        .map(UserScore::toString)
                        .collect(Collectors.joining(","))
        ).orElseGet(() -> "");
    }
}
